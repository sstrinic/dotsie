import os

from dotenv import load_dotenv

load_dotenv()
hostIP = os.getenv("ROUTER_IP")
username = os.getenv("MT_USERNAME")
dns1 = os.getenv("PIHOLE_IP")
dns2 = os.getenv("ROUTER_IP")

mikrotik = {
    "device_type": "mikrotik_routeros",
    "host": hostIP,
    "username": username,
    "use_keys": True,
    "key_file": ".ssh/id_rsa.pub",
}

dns = (dns1, dns2)
