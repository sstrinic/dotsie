#!/bin/bash
#
# Helper script to print specified lines or ranges from a file.
# Lines should be specified by a single number,
# and ranges can be specified using a hyphen or a comma(e.g., 4-10).
# USAGE: line.sh 3 7 4-10 25,32 filename
#

if [ "$#" -lt 2 ]; then
  echo "Usage: $0 line1 [line2 ... range] filename"
  exit 1
fi

line=""

for arg in "$@"; do

  if [ "$arg" == "${@: -1}" ]; then
    break
  fi

  line+="-e ${arg}p "
done

line=$(echo "$line" | sed 's/\([0-9]\)-\([0-9]\)/\1,\2/g')

sed -n $line "${@: -1}"
