from colorama import Back, Fore, Style


# change colors to title,subtitle,text..warn,err
def coloredPrint(color, text):
    if color == "red":
        print(Fore.RED + Style.BRIGHT + Back.BLACK)
        print(text)
        print(Style.RESET_ALL)
    elif color == "green":
        print(Fore.GREEN, end="")
        print(text)
        print(Style.RESET_ALL, end="")
    elif color == "accentGreen":
        print("\n" + Fore.GREEN + Style.BRIGHT + Back.BLACK, end="")
        print(text, end="")
        print(Style.RESET_ALL)
    elif color == "accentWhite":
        print(Fore.WHITE + Style.BRIGHT + Back.BLUE)
        print(text, end="")
        print(Style.RESET_ALL)
    elif color == "white":
        print(Fore.WHITE + Back.BLUE, end="")
        print(text, end="")
        print(Style.RESET_ALL)


def main():
    pass


if __name__ == "__main__":
    main()
