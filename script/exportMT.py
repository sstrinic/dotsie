import arrow
from _router import mikrotik
from netmiko import ConnectHandler

time = arrow.now("CET")
time = time.format("YYYY_MM_DD")

mikrotik_connection = ConnectHandler(**mikrotik)

output = mikrotik_connection.send_command("/export", cmd_verify=True)
with open("MT-export-" + time, "w") as f:
    f.write(output)
