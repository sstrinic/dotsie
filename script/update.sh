#!/bin/bash

. "$(dirname "$0")"/../tools/sourcer.sh

if [ "${PCMGR}" == "dnf" ]; then
    sudo dnf upgrade
elif [ "${PCMGR}" == "yum" ]; then
    sudo yum upgrade
elif [ "${PCMGR}" == "apt" ]; then
    sudo apt update && sudo apt upgrade
else
    echo -e "${RED}\tYOU NO USE YUM/DNF/APT.... Dewit alone or dont.${NC}\n"
fi
