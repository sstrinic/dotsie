pull:
	git -C $(shell pwd) pull

install:
	./tools/install.sh $(y)

update:
	./tools/update.sh

clean:
	./tools/clean.sh

key:
	./tools/addSSH2GIT.sh

clone:
	python3 ./tools/cloneGIT.py

.PHONY: pull install update clean key clone
