#                 #
#  A       S      #
#    L   A   E    #
#      I       S  #
#                 #
#------SYS--------#
 alias dewit="$HOME/.dotsie/script/update.sh"
 alias la="ls -a"
 alias vpn="sudo wg-quick up client1"
 alias vpnd="sudo wg-quick down client1"
 alias nord="sudo nordvpn connect"
 alias nordd="sudo nordvpn disconnect"
 alias temps="sensors; hddtemp"
 alias exa="exa -abghHl"
 alias line="$HOME/.dotsie/script/line.sh"
 alias ..="cd .."
 alias ...="cd ../.."

#-----PYTHON------#
 alias py=python3
 alias nosetests="nosetests -v"
 alias activate=". $HOME/.venvs/lpthw/bin/activate"

#-------GIT-------#
 alias log="git log --graph --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%an%C(reset)%C(bold yellow)%d%C(reset) %C(dim white)- %s%C(reset)' --all"
 alias puller="$HOME/.dotsie/script/puller.sh"

#-----PROJECTS----#
 alias lab="cd $HOME/Documents/GitLab"
 alias hub="cd $HOME/Documents/GitHub"
 alias dott="cd $HOME/.dotsie"

#-----ANSIBLE-----#
 alias yall="ansible-playbook $HOME/.dotsie/ansible/playbooks/update_all.yaml -K -i $HOME/.dotsie/ansible/inventory/inventory.yaml"
 alias cdk="npx aws-cdk"
