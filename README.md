# Dotsie

**WARNING:** Repo is not done!! This message will be removed when this repo gets fully tested.  

Dotfiles, scripts and other files for using local machine and HomeLab.  
Makes ~~cool~~ repo, refuses to elaborate further.

```ascii
     ____        __       _
    / __ \____  / /______(_)__
   / / / / __ \/ __/ ___/ / _ \
  / /_/ / /_/ / /_(__  ) /  __/
 /_____/\____/\__/____/_/\___/
```

## Structure

- **ansible/** - Ansible related files
- **data/** - configs for programms & shell
- **script/** - sh & py scripts
- **tools/** - dotfiles install/update/clean scripts
- **requirements.txt** - python packages needed 2 be installed
- **configure.sh** - script to be run first time only
