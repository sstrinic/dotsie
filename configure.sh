#!/bin/bash
#     ____        __       _    
#    / __ \____  / /______(_)__ 
#   / / / / __ \/ __/ ___/ / _ \
#  / /_/ / /_/ / /_(__  ) /  __/
# /_____/\____/\__/____/_/\___/ 
#

. "$(dirname "$0")/tools/sourcer.sh"
find . -name \*sh -exec sudo chmod +x {} \; &&
    echoGreen "CHMOD SUCCESS" "shell files now executable"

if [ "${PCMGR}" == "dnf" ]; then
    sudo dnf -y install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-"$(rpm -E %fedora)".noarch.rpm
    sudo dnf -y install https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-"$(rpm -E %fedora)".noarch.rpm
    sudo dnf config-manager --enable fedora-cisco-openh264
    sudo dnf check-update 1> /dev/null && sudo sudo dnf --skip-broken -y install make curl wget dnf-plugins-core
    sudo dnf config-manager --add-repo https://rpm.releases.hashicorp.com/fedora/hashicorp.repo
    sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
    sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
    sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
    sudo dnf install -y https://packages.microsoft.com/config/rhel/9.0/packages-microsoft-prod.rpm
elif [ "${PCMGR}" == "yum" ]; then
    sudo yum -y install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-"$(rpm -E %fedora)".noarch.rpm
    sudo yum -y install https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-"$(rpm -E %fedora)".noarch.rpm
    sudo yum config-manager --enable fedora-cisco-openh264
    sudo yum check-update 1> /dev/null && sudo yum --skip-broken -y install make curl wget
    sudo yum config-manager --add-repo https://rpm.releases.hashicorp.com/fedora/hashicorp.repo
    sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
    sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
    sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
    sudo yum install -y https://packages.microsoft.com/config/rhel/9.0/packages-microsoft-prod.rpm
elif [ "${PCMGR}" == "apt" ]; then
    sudo apt update 1> /dev/null && sudo apt -y install make curl wget gpg apt-transport-https
    wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
    sudo install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg
    sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
    rm -f packages.microsoft.gpg
    wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
    gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
else
    echo -e "${RED}\tYOU NO USE YUM/DNF/APT.... Dewit alone or dont.${NC}\n"
fi

if [ ! -d "${DF_DIR}" ]; then
    mkdir "${DF_DIR}" &&
    cp -ri "$(dirname "$0")"/* "${DF_DIR}" &&
    echoGreen "COPIED DOTSIE" "DF copied to .dotsie"
else
    echoGreen "DOTSIE in place" "ready to install!\n\tRun: make install"
fi
