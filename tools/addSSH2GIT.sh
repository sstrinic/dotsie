#!/bin/bash
#     ____        __       _    
#    / __ \____  / /______(_)__ 
#   / / / / __ \/ __/ ___/ / _ \
#  / /_/ / /_/ / /_(__  ) /  __/
# /_____/\____/\__/____/_/\___/ 
#

. "$(dirname "$0")/sourcer.sh"

if [[ ! -d "${HOME}/.ssh/" ]]; then
    mkdir "${HOME}/.ssh/"
    chmod 700 "${HOME}/.ssh/"
fi

if [[ -f "${HOME}/.ssh/id_ed25519" ]]; then
    echoYellow "SSH SKIP" "SSH Key already exists. Skipping SSH Key Generation."
    if [[ ! $1 == "skip" ]]; then
        echoRed "Public Key:"
        cat "${HOME}"/.ssh/id_ed25519.pub
    fi
    exit 0
fi

# -C set email, -N empty passcode, -q quiet
ssh-keygen -q -t ed25519 -f "${HOME}"/.ssh/id_ed25519 -N ""

sshagent="ssh-agent -s"
eval "${sshagent}"

# touch ~/.ssh/config
# printf "Host *\n AddKeysToAgent yes\n UseKeychain yes\n IdentityFile ~/.ssh/id_ed25519" | tee ~/.ssh/config

sudo ssh-add -K "${HOME}"/.ssh/id_ed25519 &&
    echoGreen "KEY ADDED" "SSH Key created and added to agent. Add public key to remote (GitHub, GitLab, etc.)"
echoRed "Public Key:"
cat "${HOME}"/.ssh/id_ed25519.pub

# echoGreen "run 'cat ~/.ssh/id_ed25519.pub | xclip -selection clipboard' and paste that into Git[Hub|Lab]"

firefox -new-tab -url "https://github.com/settings/keys" -new-tab -url "https://gitlab.com/-/user_settings/ssh_keys"