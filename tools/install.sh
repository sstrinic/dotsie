#!/bin/bash
#     ____        __       _    
#    / __ \____  / /______(_)__ 
#   / / / / __ \/ __/ ___/ / _ \
#  / /_/ / /_/ / /_(__  ) /  __/
# /_____/\____/\__/____/_/\___/ 
#
# WARNING!!
# Run this script only first time, later use update script.
# Otherwise you will set everything to default/initial state.

set -u

. "$(dirname "$0")/sourcer.sh"

[[ "$(read -er -p 'PLEASE CONFIRM!!! [y/N]> '; echo "$REPLY")" == [Yy]* ]] && echoGreen "\n\tHERE WE GOOOOO..." || exit 0

. "$(dirname "$0")/packages.cfg"

POSTMAN_URL="https://dl.pstmn.io/download/latest/linux_64"
TMP="$(mktemp -d)"
LOGFILE="$TMP"/log.txt

clean() { rm -rf "$TMP"; }
trap clean EXIT

#####################
# INSTALLING PACKAGES
#####################

echoYellow "[i-1]" "...Installing packages with PM: ${GREEN}${PCMGR}${NC}"

if [ "${PCMGR}" == "dnf" ]; then
    sudo dnf check-update 1> /dev/null && sudo dnf 1> /dev/null --skip-broken -y install \
        $SHARED_PCKGS \
        $DNF_PCKGS
elif [ "${PCMGR}" == "yum" ]; then
    sudo yum check-update 1> /dev/null && sudo yum 1> /dev/null --skip-broken -y install \
        $SHARED_PCKGS \
        $YUM_PCKGS
elif [ "${PCMGR}" == "apt" ]; then
    sudo apt update 1> /dev/null && sudo apt 1> /dev/null -y install \
        $SHARED_PCKGS \
        $APT_PCKGS
    echoYellow "\n[i-1-1]" "...Installing azure-cli for Debian"
    curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
else
    echoRed "\tYOU NO USE YUM/DNF/APT.... Dewit alone or dont.\n" 1>&2
fi

echoYellow "[i-2]" "...Installing Postman"

if ! command -v postman &> /dev/null; then
    if command -v curl &> /dev/null; then
        curl --location --retry 10 --output "$TMP/postman.tar.gz" "$POSTMAN_URL"
    elif command -v wget &> /dev/null; then
        wget --output-document "$TMP/postman.tar.gz" "$POSTMAN_URL"
    else
        echoRed "\tYou need either cURL or wget installed on your system" 1>&2
        exit 1
    fi
    tar --directory "$TMP" -xzf "$TMP/postman.tar.gz"
	sudo cp -uR "$TMP"/Postman /opt/
	sudo rm /usr/local/bin/postman 2> /dev/null
    sudo ln -s /opt/Postman/Postman /usr/local/bin/postman &&
    sudo cp -u "${DF_DIR}"/data/postman.desktop /usr/share/applications/postman.desktop &&
        echoGreen "\t✔ POSTMAN INSTALL" " success"
else
    echoYellow "\t✔ POSTMAN SKIPPED" " installed already"
fi

echoYellow "[i-3]" "...Installing packages with Pip3"
if pip3 install -r "${DF_DIR}/requirements.txt" > /dev/null; then echoGreen "\t✔ PIP3 INSTALL" "success modules"; 
    else echoRed "\tPIP3 NOT SUCCESFUL!!" 1>&2; fi

echoYellow "[i-4]" "...Installing NordVPN"
sh <(curl -sSf https://downloads.nordcdn.com/apps/linux/install.sh)


## uninstall existing from yum then check and skip if exists
echoYellow "[i-5]" "...Installing aws-cli"
if [ ! "${PCMGR}" == "dnf" ] && [ ! "${PCMGR}" == "yum" ]; then
    sudo ${PCMGR} remove aws &> /dev/null
fi
if ! command -v aws &> /dev/null; then
    curl --location --retry 10 --output "$TMP/awscliv2.zip" "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip"
    unzip "$TMP"/awscliv2.zip
    sudo "$TMP"/aws/install
    rm -rf "$TMP"/aws
    rm "$TMP"/awscliv2.zip
else
    echoYellow "\t✔ AWS-CLI SKIPPED" "installed already"
fi

#####################
# INITIAL ONLY CREATION
#####################

function makeDir {
    file="$1"
    if [[ ! -d "$file" ]]; then
        mkdir "$file" > /dev/null 2>&1 &&
            echoGreen "\t✔ DIR CREATE" "$file created..."
    else
        echoYellow "\tIGNORE" "$file dir found, ignoring..."
    fi
}

echoYellow "[i-4]" "...Making directories"

makeDir "${HOME}"/Documents/GitHub
makeDir "${HOME}"/Documents/GitLab
makeDir "${HOME}"/Insider

echoYellow "[i-5]" "...Deleting existing files"

echoYellow "\t[i-5-1]" "...Deleting Firefox"
rm -rf ~/.mozilla/firefox/"${FF_DIR}"/* > /dev/null 2>&1 &&
echoGreen "\t\t✔ Deleted Firefox initial configs"

echoYellow "\t[i-5-2]" "...Copying only inital files"

echoGreen "\t\t✔ Copying Firefox initial data"
unzip "${DF_DIR}"/data/FF-profile.zip -d ~/.mozilla/firefox/"${FF_DIR}" 1> /dev/null &&
cp -r ~/.mozilla/firefox/"${FF_DIR}"/FF-profile/* ~/.mozilla/firefox/"${FF_DIR}"/ &&
rm -rf ~/.mozilla/firefox/"${FF_DIR}"/FF-profile &&
echoGreen "\t\t✔ Deleted Firefox"

#####################
# SCRIPT FOR MULTI CHG
#####################

bash "${DF_DIR}/tools/update.sh"

#####################
# GENERATE SSH KEY GIT
#####################

bash "${DF_DIR}/tools/addSSH2GIT.sh" skip

#####################
# MANUAL TASKS
#####################

echoYellow "[END]" "Some of steps needs to be done manualy: Ublock list, VSCode conf & Flatpak packages"
