#!/bin/bash
#     ____        __       _    
#    / __ \____  / /______(_)__ 
#   / / / / __ \/ __/ ___/ / _ \
#  / /_/ / /_/ / /_(__  ) /  __/
# /_____/\____/\__/____/_/\___/ 
#

. "$(dirname "$0")/sourcer.sh"

echoRed "CLEANING...."
[[ "$(read -er -p 'PLEASE CONFIRM!!! [y/N]> '; echo "$REPLY")" == [Yy]* ]] && echoGreen "Continuing" || exit 0

unLink() {
    file="$1"
    if [ -L ~/"$file" ]; then
        rm -vf "$file"  &&
        echoGreen "\t✔ Deleted" "link $file"
    else
        echoGreen "\tNOT FOUND" "$file link not found.."
    fi
}

cpFile() {
    file="$1"
    if [ ! -e ~/"$file" ]; then
        cp -f "${DF_DIR}"/data/home/"$file" "${HOME}"/"$file" &&
        echoGreen "\t✔ Copying" "$file"
    else
        echoYellow "\tFound" "$file found, ignoring..."
    fi
}

if [[ $1 == "all" ]]; then
    unLink .bashrc
    unLink .bash_aliases
    unLink .gitconfig
    unLink .nanorc

    cpFile .bashrc
    cpFile .bash_aliases
    cpFile .gitconfig
    cpFile .nanorc

    rm -rf "${HOME}/.dotsie"
    if cd "${HOME}"; then echo "DONE"; else echo "FAIL to cd ${HOME}"; fi
fi

rm "${DF_DIR}"/tools/install.sh
rm "${DF_DIR}"/tools/addSSH2GIT.sh
