import os
import sys

import git
import yaml
from git.exc import GitCommandError
from pydantic import BaseModel, ValidationError

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/..")
from script._addons import coloredPrint  # noqa:E402


class GitRepo(BaseModel):
    name: str
    url: str
    git: str
    proto: str = None
    destination: str = None


# Append new projects to puller script, but check if it already exists
def updatePullerScript(gitproject: GitRepo):
    dest = gitproject.destination.replace(homeDir, "~")
    with open(pullerScript, mode="a+") as file:
        file.seek(0)
        if dest in file.read():
            return
        print("%s%s%s" % ('echo -e "${GREEN}', gitproject.name, '...${NC}"'), file=file)
        print("%s %s %s" % ("git -C", dest, "pull"), file=file)
    coloredPrint("white", "Puller script updated with new script at end...")


# Check if project is already cloned and clone if not with https/git
def cloneProject(gitproject: GitRepo):
    print("Checking if project already exists and if there is dest dir...")
    if os.path.isdir(gitproject.destination):
        if not os.listdir(gitproject.destination):
            print("Directory is empty, starting to clone...")
        else:
            coloredPrint("green", "Already cloned... " + gitproject.destination)
            return
    else:
        print("Given directory doesn't exist, cloning to " + gitproject.destination)

    if gitproject.proto == "ssh":
        repo_url = gitproject.url.replace(
            f"https://{gitproject.git}.com", f"git@{gitproject.git}.com:"
        )
    try:
        git.Repo.clone_from(repo_url, gitproject.destination)
        print(f"Cloned {repo_url} to {gitproject.destination}")
        updatePullerScript(gitproject)
    except GitCommandError as e:
        print(f"Error cloning {repo_url}: {e}")


# Check destination dir and create new dir if it's specified in yaml,
# if not specified then cloning will create dest dir with name specified in yaml
def destinationDir(gitProject: GitRepo) -> str:
    destinationDir = gitProject.destination
    if not destinationDir:
        if gitProject.git == "gitlab":
            destinationDir = defaultDir + "/GitLab/" + gitProject.name
        elif gitProject.git == "github":
            destinationDir = defaultDir + "/GitHub/" + gitProject.name
        else:
            raise GitCommandError("ERR: Git not recognised/defined...")
    else:
        if destinationDir.startswith("~"):
            destinationDir = destinationDir.replace("~", homeDir, 1)
        if not os.path.exists(destinationDir):
            os.makedirs(destinationDir)
        return destinationDir
    return destinationDir


# Saving project info in object, call dir check and clone
def setupProjectInfo(git: str, project: dict):
    try:
        gitProject = GitRepo(**project, git=git)
    except ValidationError as e:
        print(e.errors())
    gitProject.destination = destinationDir(gitProject)
    coloredPrint("accentGreen", gitProject.name)
    cloneProject(gitProject)


# Go through yaml file for projects 2b cloned depending on git provider
def cloneProjectsFromYaml(yaml_file: str):
    with open(yaml_file, "r") as file:
        data = yaml.safe_load(file)

    # Go through different gits
    for service, projects in data.items():
        project_no = len(projects)
        count = 0
        coloredPrint(
            "accentWhite",
            "CLONING " + service.upper() + " -> " + str(count) + "/" + str(project_no),
        )
        # Each project in list, for each git
        for project in projects:
            setupProjectInfo(service.lower(), project=project)
            count += 1
            coloredPrint("white", "Cloned: " + str(count) + "/" + str(project_no))


if __name__ == "__main__":
    homeDir = os.environ["HOME"]
    defaultDir = homeDir + "/Documents"
    scriptDir = os.path.dirname(os.path.realpath(__file__))
    yaml_file = scriptDir + "/git.yaml"
    pullerScript = scriptDir + "/../script/puller.sh"

    cloneProjectsFromYaml(yaml_file)
