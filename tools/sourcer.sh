#!/bin/bash
#     ____        __       _    
#    / __ \____  / /______(_)__ 
#   / / / / __ \/ __/ ___/ / _ \
#  / /_/ / /_/ / /_(__  ) /  __/
# /_____/\____/\__/____/_/\___/ 
#

FF_DIR=$(awk -F= '/^Path=.*release.*/ {print $2}' ~/.mozilla/firefox/profiles.ini)
echo "$FF_DIR"
export FF_DIR
export DF_DIR="${HOME}/.dotsie"
export RED='\033[1;31m'
export GREEN='\033[1;32m'
export YELLOW='\033[1;33m'
export NC='\033[0m'

if command -v dnf &> /dev/null; then
    export PCMGR="dnf"
elif command -v yum &> /dev/null; then
    export PCMGR="yum"
elif command -v apt &> /dev/null; then
    export PCMGR="apt"
else
    echo -e "${RED}\tNo package manager found!${NC}\n"
    export PCMGR=""
fi

echoGreen() {
  echo -e "${GREEN}$1${NC} $2"
}

echoYellow() {
  echo -e "${YELLOW}$1${NC} $2"
}

echoRed() {
  echo -e "${RED}$1${NC} $2"
}

echoExit() {
  echo -e "${RED}$1${NC} $2"
  exit 1
}

logger() {
    echo "LOGGED"
}