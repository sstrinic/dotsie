#!/bin/bash
#     ____        __       _
#    / __ \____  / /______(_)__
#   / / / / __ \/ __/ ___/ / _ \
#  / /_/ / /_/ / /_(__  ) /  __/
# /_____/\____/\__/____/_/\___/
#

. "$(dirname "$0")/sourcer.sh"

linkdotfile() {
    file="$1"
    if [ ! -e ~/"$file" ] && [ ! -L ~/"$file" ]; then
        if [ -e "${DF_DIR}/data/home/$file" ]; then
            echoGreen "\t✔ LINKING" "$file found, but no link..."
            ln -s "${DF_DIR}"/data/home/"$file" ~/"$file"
        else
            echoRed "\tNOT FOUND" "$file in dotfiles..."
        fi
    else
        echoYellow "\tIGNORE" "$file link found..."
    fi
}

removeFile() {
    file="$1"
    if [ -e ~/"$file" ] && [ ! -L ~/"$file" ]; then
        sudo rm -f ~/"$file" > /dev/null 2>&1 &&
        echoGreen "\t✔ REMOVE" "$file found, REMOVING..."
    else
        echoYellow "\tIGNORE" "$file file not found, ignoring..."
    fi
}

#####################
# VISUAL APPEREANCE
#####################

# Set appereance to dark mode
if [ "${PCMGR}" == "apt" ]; then
    gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'
else
    gsettings set org.gnome.desktop.interface gtk-theme 'HighContrastInverse'
fi
gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'

# Fedora- tested
dconf load /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/ < "${DF_DIR}"/data/custom-keybindings.conf

#####################
# DELETING EXISTING DF & LINKING
#####################

echoYellow "[u-1]" "...Removing existing dotfiles"

removeFile .bashrc
removeFile .bash_aliases
removeFile .gitconfig
removeFile .nanorc

echoYellow "[u-2]" "...Linking new files"

linkdotfile .bashrc
linkdotfile .bash_aliases
linkdotfile .gitconfig
linkdotfile .nanorc

#####################
# COPYING DIRS & FILES
#####################

echoYellow "[u-3]" "...Copying changeable files to host"

cp -uRi "${DF_DIR}"/data/home/.config/* "${HOME}"/.config &&
    echoGreen "\t✔ COPY" ".config dir"

cp -u "${DF_DIR}"/data/home/.config/git-prompt.sh "${HOME}"/.config/git-prompt.sh &&
    echoGreen "\t✔ Copying git-prompt.sh"


# echoYellow "[u-4]" "...Updating aws-cli"
# curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
# unzip awscliv2.zip
# sudo ./aws/install --bin-dir /usr/local/bin --install-dir /usr/local/aws-cli --update
# rm -rf aws
# rm awscliv2.zip