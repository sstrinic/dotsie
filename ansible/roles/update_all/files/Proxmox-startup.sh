#!/bin/bash

GREEN='\033[0;32m'
NOCOLOR='\033[0m' 
CONFIG="/etc/corosync/corosync.conf"
if [ ! -e $CONFIG ] ; then
    echo "ERROR: File ${CONFIG} is missing!!"
    exit;
fi

VOTES=$(awk '$2 ~ /172\.16\.10\.60/ {print a}{a=$2}' $CONFIG)
LNR=$(awk '$2 ~ /172\.16\.10\.60/ {print NR-1}{a=$2}' $CONFIG)
if [[ $VOTES -eq 2 ]] ; 
  then
    echo "Quorum set to ${VOTES} already"
    exit;
  else
    sed -i "${LNR}s/.*/    quorum_votes: 2/" $CONFIG
    echo -e "\t${GREEN}SUCCESSFUL!!\n\tVotes changed to 2\n\tRestarting CoroSync...\n${NOCOLOR}"
    systemctl restart corosync.service && pvecm status
fi
